var Events = require('events');
var eventEmitter = new Events.EventEmitter();
var {Sapa, Jawab} = require('./DayExport');
var {Persons, NamaTempatPersons} = require('./forExport2');


eventEmitter.addListener('Hai',Persons);
eventEmitter.addListener('Hello',Jawab);
eventEmitter.addListener('Pulang', NamaTempatPersons);

eventEmitter.emit('FirstEvent', 'This')

eventEmitter.emit('Hai');
console.log("")
eventEmitter.emit('Pulang')
