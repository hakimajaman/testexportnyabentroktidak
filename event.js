var events = require('events');
var { persons } = require('./forExport.js');

var eventEmitter = new events.EventEmitter();

function callName(){
    for (let person of persons) {
        console.log("Hi " + person.name)
    }
}

function backHome(){
    for (let person of persons) {
        console.log(person.name + " back to " + person.add)
    }
}

eventEmitter.on('greet', () => {
    callName()
    console.log("---'greet' event occurred!")
});

eventEmitter.addListener('return', () => {
    backHome()
    console.log("---'return' event occurred!")
})

eventEmitter.emit('greet');
eventEmitter.emit('return');



