var person  = [
    {
        name: "Fikri",
        alamat: "Solo"
    },
    {
        name: "Bram",
        alamat: "Malang"
    },
    {
        name: "Hakim",
        alamat: "Batam"
    },
    {
        name: "Ayu",
        alamat: "Batam"
    },
    {
        name: "Nasha",
        alamat: "Batam"
    }
];

function Persons(){
    for (var orang of person) {
        console.log("Hai " + orang.name);
    }
}

function Tempats(){
    for (var tempat of person) {
        console.log(tempat.alamat);
    }
}

function NamaTempatPersons(){
    for (var NamePlace of person) {
        console.log(NamePlace.name + " Pulang Ke " + NamePlace.alamat)
    }
}

module.exports.Persons = Persons;
module.exports.Tempats = Tempats;
module.exports.NamaTempatPersons = NamaTempatPersons